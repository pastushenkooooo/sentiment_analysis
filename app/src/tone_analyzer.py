from dostoevsky.tokenization import RegexTokenizer
from dostoevsky.models import FastTextSocialNetworkModel
from src.models import ResultType


def get_tone(messages):
    FastTextSocialNetworkModel.MODEL_PATH = "/app/src/fasttext-social-network-model.bin"
    tokenizer = RegexTokenizer()
    model = FastTextSocialNetworkModel(tokenizer=tokenizer)
    results = model.predict(messages, k=2)
    res_type = max(results[0], key=results[0].get)
    return ResultType[res_type]
