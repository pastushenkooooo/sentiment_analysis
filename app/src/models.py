from datetime import date, datetime
from typing import Optional, List
from pydantic import BaseModel
import enum


class Phones(BaseModel):
    phone: str
    id: int
    name: str
    joined: date


class User(BaseModel):
    id: int
    name: str
    joined: date
    is_admin: bool = False
    nickname: Optional[str]
    phones: List[Phones]


class UserResponse(BaseModel):
    id: str


class CreateUser(BaseModel):
    name: str
    surname: str
    birthdate: Optional[date]
    phone: Optional[int]


class UserDB(CreateUser):
    id: int
    created: datetime

    class Config:
        orm_mode = True


class ResultType(enum.Enum):
    positive = "Положительная тональность"
    negative = "Отрицательная тональность"
    neutral = "Нейтральная тональность"
    skip = "Неудалось определить тональность"
    speech = "Формальное поздравление"


class History(BaseModel):
    id: Optional[int] = None
    text: str
    result: str
    created: Optional[datetime] = None
