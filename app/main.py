from fastapi import FastAPI, Body, Request, Form, Depends, HTTPException
from fastapi.responses import RedirectResponse, FileResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

from src.models import ResultType, History
from db.alchemy import check_user_auth, save_text, Session, get_analyzing_history, get_popular_texts
from src.tone_analyzer import get_tone
from db.session import get_db

app = FastAPI()

templates = Jinja2Templates(directory="src/templates")

app.mount("/static", StaticFiles(directory="src/static"), name="static")


@app.get("/")
async def return_redirect():
    return RedirectResponse("/login")


@app.get("/login")
async def return_index():
    return FileResponse("src/templates/login.html")


@app.post("/form/")
async def login(request: Request, username: str = Form(), password: str = Form(), db: Session = Depends(get_db)):
    user = check_user_auth(db, username, password)
    if not user:
        raise HTTPException(status_code=403, detail="Incorrect login or password")

    return FileResponse("src/templates/index.html")


@app.post("/form/api/analyze-text")
async def analyze_text(data=Body(), db: Session = Depends(get_db)):
    result_type = get_tone([data["analyzedText"]])
    save_text(db, data["analyzedText"], result_type.name)
    return result_type.value


@app.get("/form/api/popular")
async def get_popular(db: Session = Depends(get_db)):
    popular = get_popular_texts(db)
    list_of_popular = []
    for el in popular:
        history_model = History(text=el.text, result=ResultType[el.result].value)
        list_of_popular.append(history_model)
    return list_of_popular


@app.get("/form/api/history")
async def get_history(db: Session = Depends(get_db)):
    history = get_analyzing_history(db)
    list_of_history = []
    for el in history:
        history_model = History(id=el.id, text=el.text, result=ResultType[el.result].value, created=el.created)
        list_of_history.append(history_model)
    return list_of_history
