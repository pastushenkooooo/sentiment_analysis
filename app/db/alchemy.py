from sqlalchemy.orm import Session
from db.db_models import History, UserInfo, User
from sqlalchemy import func
from sqlalchemy import text


def check_user_auth(db: Session, username: str, password: str):
    return (
        db.query(UserInfo)
        .join(User, UserInfo.user_id == User.id)
        .filter(User.username == username, User.password == password)
        .one_or_none()
    )


def save_text(db: Session, text: str, result: str):
    history = History()
    history.text = text
    history.result = result
    history.user_id = 1
    db.add(history)
    db.commit()


def get_popular_texts(db: Session):
    return (
        db.query(History.text, History.result, func.count(History.text).label("count"))
        .group_by(History.text, History.result)
        .order_by(text('count DESC'))
        .limit(5)
        .all()
    )


def get_analyzing_history(db: Session):
    return (
        db.query(History)
        .order_by(text('created DESC'))
        .limit(5)
        .all()
    )
