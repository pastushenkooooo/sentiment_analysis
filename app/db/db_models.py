from sqlalchemy import (
    DateTime,
    Column,
    Integer,
    VARCHAR,
    DATE,
    ForeignKey,
)
from sqlalchemy.sql import func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.mysql import MEDIUMTEXT

Base = declarative_base()


class History(Base):
    __tablename__ = "history"
    id = Column(Integer, primary_key=True, index=True)
    text = Column(MEDIUMTEXT(charset='utf8mb4'), comment="Анализируемый текст")
    user_id = Column(Integer, ForeignKey("users.id"))
    result = Column(VARCHAR(255), nullable=False, comment="Полученный результат")
    created = Column(DateTime, server_default=func.now(), comment="Дата создания")


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(VARCHAR(255), nullable=False, comment="Логин пользователя")
    password = Column(VARCHAR(255), nullable=False, comment="Пароль пользователя")
    created = Column(DateTime, server_default=func.now(), comment="Дата рождения")


class UserInfo(Base):
    __tablename__ = "users_info"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    name = Column(VARCHAR(255), nullable=False, comment="Имя пользователя")
    surname = Column(VARCHAR(255), nullable=False, comment="Фамилия пользователя")
    birthdate = Column(DATE, comment="Дата рождения")
    phone = Column(Integer, comment="Номер телефона")
